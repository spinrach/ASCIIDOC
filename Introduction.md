---
layout: default
title: Introduction
sidebar: mydoc_sidebar
folder: /
permalink: index.html
companyname: XM Cyber
---
# Introduction

Welcome to the {{page.companyname}} docs.
TIP: We manage docs from GitHub. Open a pull request anytime if you have suggestions or requests.

The current version of {{page.companyname}} is {appversion}. 

{toc}

## Conventions

Document conventions: 

TIP: this is a tip

## Audience

| Parameter | Description | Example |
| --------- | ----------- | ------- |
|           |             |         |
|           |             |         |
|           |             |         |

